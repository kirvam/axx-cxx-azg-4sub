module "project-core" {
  source = "./core"
}

module "stage-1-build-nsgs" {
  source = "./stage-1/vnets/nsgs/resources"

  vnet-location = module.project-core.rg-location
  vnet-rg       = module.project-core.cvo-rg-name
}

module "stage-1-build-vnets" {
  source = "./stage-1/vnets/core"

  vnet-location = module.project-core.rg-location
  vnet-rg       = module.project-core.cvo-rg-name
}

module "stage-1-build-subnets" {
  source = "./stage-1/vnets/subnets"

  cvo-vnet-name = module.stage-1-build-vnets.cvo-vnet-name
  vnet-rg       = module.project-core.cvo-rg-name

  dmz-nsg-id     = module.stage-1-build-nsgs.dmz-nsg-id
  fcr-nsg-id     = module.stage-1-build-nsgs.fcr-nsg-id
  prod-nsg-id    = module.stage-1-build-nsgs.prod-nsg-id
  staging-nsg-id = module.stage-1-build-nsgs.staging-nsg-id
  test-nsg-id    = module.stage-1-build-nsgs.test-nsg-id
  files-nsg-id   = module.stage-1-build-nsgs.files-nsg-id
}

module "stage-1-nsg-rules" {
  source = "./stage-1/vnets/nsgs/rules"

  vnet-location = module.project-core.rg-location
  vnet-rg       = module.project-core.cvo-rg-name

  dmz-cidr     = module.stage-1-build-subnets.dmz-cidr
  fcr-cidr     = module.stage-1-build-subnets.fcr-cidr
  test-cidr    = module.stage-1-build-subnets.test-cidr
  staging-cidr = module.stage-1-build-subnets.staging-cidr
  prod-cidr    = module.stage-1-build-subnets.prod-cidr
  files-cidr   = module.stage-1-build-subnets.files-cidr
}

module "stage-1-associate-nsgs" {
  source = "./stage-1/vnets/nsgs/associations"

  dmz-nsg-id     = module.stage-1-build-nsgs.dmz-nsg-id
  fcr-nsg-id     = module.stage-1-build-nsgs.fcr-nsg-id
  prod-nsg-id    = module.stage-1-build-nsgs.prod-nsg-id
  staging-nsg-id = module.stage-1-build-nsgs.staging-nsg-id
  test-nsg-id    = module.stage-1-build-nsgs.test-nsg-id
  files-nsg-id   = module.stage-1-build-nsgs.files-nsg-id

  dmz_subnet_id     = module.stage-1-build-subnets.dmz_subnet_id
  fcr_subnet_id     = module.stage-1-build-subnets.fcr_subnet_id
  prod_subnet_id    = module.stage-1-build-subnets.prod_subnet_id
  staging_subnet_id = module.stage-1-build-subnets.staging_subnet_id
  test_subnet_id    = module.stage-1-build-subnets.test_subnet_id
  files_subnet_id   = module.stage-1-build-subnets.files_subnet_id
}

module "stage-1-build-vms" {
  source = "./stage-1/vms/machines"

  vm-location = module.project-core.rg-location
  vm-rg       = module.project-core.cvo-rg-name

  machine_size = module.project-core.machine_size

  diag-storage-account = module.project-core.diag-storage-account

  admin_username = module.project-core.admin_username
  admin_password = module.project-core.admin_password

  image_publisher = module.project-core.image_publisher
  image_offer     = module.project-core.image_offer
  image_sku       = module.project-core.image_sku
  image_version   = module.project-core.image_version

  fcr_subnet_id   = module.stage-1-build-subnets.fcr_subnet_id
  test_subnet_id  = module.stage-1-build-subnets.test_subnet_id
  files_subnet_id = module.stage-1-build-subnets.files_subnet_id
}


