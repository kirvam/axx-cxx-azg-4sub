# AOT CVO subscription here
provider "azurerm" {
    subscription_id = "aaefb515-69f9-4eb0-bfcf-c3a333cb86d7"  # Use for AzureUSGovernment cloud
    tenant_id       = "1e805e77-c5e0-402d-adb5-bda9c33b3c13"  # Use for AzureUSGovernment cloud
}

resource "random_id" "rg-special" {
    byte_length = 4
}

# Resource group + location settings
variable "resource_group" {
    description = "resource group to put created resources into"
    default     = "aot-cvo"
}

variable "location" {
    description = "datacenter location to put resources into"
    default     = "usgovvirginia"  # Use for AzureUSGovernment cloud
    # default     = "eastus"  # Use for AzureCloud
}


# Start building resources
resource "azurerm_resource_group" "aot-cvo" {
    name     = "${var.resource_group}-${random_id.rg-special.hex}"
    location = "${var.location}"

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

output "cvo-rg-name" {
    value = "${azurerm_resource_group.aot-cvo.name}"
}

output "rg-location" {
    value = "${var.location}"
}

