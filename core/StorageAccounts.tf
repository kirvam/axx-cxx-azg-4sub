#Configure storage accounts
resource "azurerm_storage_account" "aot-cvo-diagnostics" {
    name = "testaotcvodiagnostics"
    resource_group_name = "${azurerm_resource_group.aot-cvo.name}"
    location = "${azurerm_resource_group.aot-cvo.location}"
    account_replication_type = "LRS"
    account_tier = "Standard"

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

output "diag-storage-account" {
    value = "${azurerm_storage_account.aot-cvo-diagnostics.primary_blob_endpoint}"
}

