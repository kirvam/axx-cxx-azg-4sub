# Keep needed machine sizes handy here
variable "machine_size" {
    type        = "map"
    description = "map of machine sizes to be used (az vm list-sizes --output=table -l usgovvirginia)"
    default     = {
        "a2v2"  = "Standard_A2_v2"
        "d4sv3" = "Standard_D4s_v3"
        "e4v3" = "Standard_E4_v3"
        "e8v3"  = "Standard_E8_v3"
    }
}

output "machine_size" {
    value = "${var.machine_size}"
}

