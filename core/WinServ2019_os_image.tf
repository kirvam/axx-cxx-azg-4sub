# OS Image settings
variable "image_publisher" {
  description = "name of the publisher of the image (az vm image list)"
  default     = "MicrosoftWindowsServer"
}

output "image_publisher" {
  value = "${var.image_publisher}"
}

variable "image_offer" {
  description = "the name of the offer (az vm image list)"
  default     = "WindowsServer"
}

output "image_offer" {
  value = "${var.image_offer}"
}

variable "image_sku" {
  description = "image sku to apply (az vm image list)"
  default     = "2019-Datacenter"
}

output "image_sku" {
  value = "${var.image_sku}"
}

variable "image_version" {
  description = "version of the image to apply (az vm image list)"
  default     = "latest"
}

output "image_version" {
  value = "${var.image_version}"
}

