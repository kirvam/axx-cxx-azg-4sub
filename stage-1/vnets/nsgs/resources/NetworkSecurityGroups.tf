variable "vnet-location" {}
variable "vnet-rg" {}


# Build empty NSG(s)
resource "azurerm_network_security_group" "aot-cvo-dmz-nsg" {
    name = "aot-cvo-dmz-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "dmz-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-dmz-nsg.id}"
}

resource "azurerm_network_security_group" "aot-cvo-fcr-nsg" {
    name = "aot-cvo-fcr-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "fcr-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-fcr-nsg.id}"
}

resource "azurerm_network_security_group" "aot-cvo-test-nsg" {
    name = "aot-cvo-test-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "test-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-test-nsg.id}"
}

resource "azurerm_network_security_group" "aot-cvo-staging-nsg" {
    name = "aot-cvo-staging-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "staging-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-staging-nsg.id}"
}

resource "azurerm_network_security_group" "aot-cvo-prod-nsg" {
    name = "aot-cvo-prod-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "prod-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-prod-nsg.id}"
}

resource "azurerm_network_security_group" "aot-cvo-files-nsg" {
    name = "aot-cvo-files-nsg"
    location = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"
}

output "files-nsg-id" {
    value = "${azurerm_network_security_group.aot-cvo-files-nsg.id}"
}

