variable "dmz-nsg-id" {}
variable "fcr-nsg-id" {}
variable "prod-nsg-id" {}
variable "staging-nsg-id" {}
variable "test-nsg-id" {}
variable "files-nsg-id" {}

variable "dmz_subnet_id" {}
variable "fcr_subnet_id" {}
variable "test_subnet_id" {}
variable "staging_subnet_id" {}
variable "prod_subnet_id" {}
variable "files_subnet_id" {}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-dmz-nsg-assoc" {
    subnet_id = "${var.dmz_subnet_id}"
    network_security_group_id = "${var.dmz-nsg-id}"
}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-fcr-nsg-assoc" {
    subnet_id = "${var.fcr_subnet_id}"
    network_security_group_id = "${var.fcr-nsg-id}"
}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-test-nsg-assoc" {
    subnet_id = "${var.test_subnet_id}"
    network_security_group_id = "${var.test-nsg-id}"
}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-staging-nsg-assoc" {
    subnet_id = "${var.staging_subnet_id}"
    network_security_group_id = "${var.staging-nsg-id}"
}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-prod-nsg-assoc" {
    subnet_id = "${var.prod_subnet_id}"
    network_security_group_id = "${var.prod-nsg-id}"
}

resource "azurerm_subnet_network_security_group_association" "aot-cvo-files-nsg-assoc" {
    subnet_id = "${var.files_subnet_id}"
    network_security_group_id = "${var.files-nsg-id}"
}

