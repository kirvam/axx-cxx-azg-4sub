variable "vnet-location" {}

variable "vnet-rg" {}

variable "dmz-cidr" {}

variable "fcr-cidr" {}

variable "test-cidr" {}

variable "staging-cidr" {}

variable "prod-cidr" {}

variable "files-cidr" {}

variable "SOV-ext-IPs" {
    description = "Public IPs that SOV network is NATed to."  
    default     = "159.105.0.0/16"
}

