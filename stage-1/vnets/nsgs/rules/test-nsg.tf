########################### "aot-cvo-test-nsg"

# resource "azurerm_network_security_rule" "test-allow-rdp-in" {
#     name = "RDP"
#     priority = 1001
#     direction = "Inbound"
#     access = "Allow"
#     protocol = "Tcp"
#     source_port_range = "*"
#     destination_port_range = "3389"
#     source_address_prefix = "${var.SOV-ext-IPs}"
#     destination_address_prefix = "*"
#     resource_group_name = "${var.vnet-rg}"
#     network_security_group_name = "aot-cvo-test-nsg"
# }

# Inbound deny:
resource "azurerm_network_security_rule" "test-deny-dmz-in" {
    name = "deny-dmz-in"
    priority = 4001
    direction = "Inbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.dmz-cidr}"
    destination_address_prefix = "${var.test-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-fcr-in" {
    name = "deny-fcr-in"
    priority = 4002
    direction = "Inbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.fcr-cidr}"
    destination_address_prefix = "${var.test-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-staging-in" {
    name = "deny-staging-in"
    priority = 4003
    direction = "Inbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.staging-cidr}"
    destination_address_prefix = "${var.test-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-prod-in" {
    name = "deny-prod-in"
    priority = 4004
    direction = "Inbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.prod-cidr}"
    destination_address_prefix = "${var.test-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-files-in" {
    name = "deny-files-in"
    priority = 4005
    direction = "Inbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.files-cidr}"
    destination_address_prefix = "${var.test-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

# Outbound deny:
resource "azurerm_network_security_rule" "test-deny-dmz-out" {
    name = "deny-dmz-out"
    priority = 4001
    direction = "Outbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.test-cidr}"
    destination_address_prefix = "${var.dmz-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-fcr-out" {
    name = "deny-fcr-out"
    priority = 4002
    direction = "Outbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.test-cidr}"
    destination_address_prefix = "${var.fcr-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-staging-out" {
    name = "deny-staging-out"
    priority = 4003
    direction = "Outbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.test-cidr}"
    destination_address_prefix = "${var.staging-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-prod-out" {
    name = "deny-prod-out"
    priority = 4004
    direction = "Outbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.test-cidr}"
    destination_address_prefix = "${var.prod-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

resource "azurerm_network_security_rule" "test-deny-files-out" {
    name = "deny-files-out"
    priority = 4005
    direction = "Outbound"
    access = "Deny"
    protocol = "*"
    source_port_range = "*"
    destination_port_range = "*"
    source_address_prefix = "${var.test-cidr}"
    destination_address_prefix = "${var.files-cidr}"
    resource_group_name = "${var.vnet-rg}"
    network_security_group_name = "aot-cvo-test-nsg"
}

