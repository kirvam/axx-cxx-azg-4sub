variable "vnet-cidr" {
  default = "10.251.0.0/24"
}

variable "vnet-location" {}
variable "vnet-rg" {}

# Build the virtual network(s)
resource "azurerm_virtual_network" "aot-cvo-vnet" {
    name                = "aot-cvo-vnet"
    address_space       = ["${var.vnet-cidr}"]
    location            = "${var.vnet-location}"
    resource_group_name = "${var.vnet-rg}"

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

output "cvo-vnet-name" {
  value = "${azurerm_virtual_network.aot-cvo-vnet.name}"
}

