# Build the external subnet(s)
resource "azurerm_subnet" "dmz" {
    name                 = "dmz"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.dmz-cidr}"
    network_security_group_id = "${var.dmz-nsg-id}"
}

output "dmz_subnet_id" {
  value = "${azurerm_subnet.dmz.id}"
}

# Build the internal subnet(s), and assign network security groups
resource "azurerm_subnet" "fcr" {
    name                 = "fcr"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.fcr-cidr}"
    network_security_group_id = "${var.fcr-nsg-id}"
}

output "fcr_subnet_id" {
  value = "${azurerm_subnet.fcr.id}"
}

resource "azurerm_subnet" "test" {
    name                 = "test"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.test-cidr}"
    network_security_group_id = "${var.test-nsg-id}"
}

output "test_subnet_id" {
  value = "${azurerm_subnet.test.id}"
}

resource "azurerm_subnet" "staging" {
    name                 = "staging"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.staging-cidr}"
    network_security_group_id = "${var.staging-nsg-id}"
}

output "staging_subnet_id" {
  value = "${azurerm_subnet.staging.id}"
}

resource "azurerm_subnet" "prod" {
    name                 = "prod"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.prod-cidr}"
    network_security_group_id = "${var.prod-nsg-id}"
}

output "prod_subnet_id" {
  value = "${azurerm_subnet.prod.id}"
}

resource "azurerm_subnet" "files" {
    name                 = "files"
    resource_group_name  = "${var.vnet-rg}"
    virtual_network_name = "${var.cvo-vnet-name}"
    address_prefix       = "${var.files-cidr}"
    network_security_group_id = "${var.files-nsg-id}"
}

output "files_subnet_id" {
  value = "${azurerm_subnet.files.id}"
}

