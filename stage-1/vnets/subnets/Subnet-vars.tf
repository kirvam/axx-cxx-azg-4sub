variable "dmz-cidr" {
  default = "10.251.0.160/27"
}

output "dmz-cidr" {
  value = "${var.dmz-cidr}"
}

variable "fcr-cidr" {
  default = "10.251.0.0/27"
}

output "fcr-cidr" {
  value = "${var.fcr-cidr}"
}

variable "test-cidr" {
  default = "10.251.0.32/27"
}

output "test-cidr" {
  value = "${var.test-cidr}"
}

variable "staging-cidr" {
  default = "10.251.0.64/27"
}

output "staging-cidr" {
  value = "${var.staging-cidr}"
}

variable "prod-cidr" {
  default = "10.251.0.96/27"
}

output "prod-cidr" {
  value = "${var.prod-cidr}"
}

variable "files-cidr" {
  default = "10.251.0.128/27"
}

output "files-cidr" {
  value = "${var.files-cidr}"
}

variable "cvo-vnet-name" {}

variable "vnet-rg" {}

variable "dmz-nsg-id" {}
variable "fcr-nsg-id" {}
variable "test-nsg-id" {}
variable "staging-nsg-id" {}
variable "prod-nsg-id" {}
variable "files-nsg-id" {}

