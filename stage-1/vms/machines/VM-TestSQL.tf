 Configure availability sets
resource "azurerm_availability_set" "aot-cvo-test-sql-avset" {
    name = "aot-cvo-test-sql-avset"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    platform_fault_domain_count  = 2
    platform_update_domain_count = 2
    managed                      = true
}

# Configure public IPs
# resource "azurerm_public_ip" "aot-cvo-test-sql-pub-ip" {
#     #count = 1
#     name                = "aot-cvo-test-sql-vm0-pub-ip"
#     location            = "${var.vm-location}"
#     resource_group_name = "${var.vm-rg}"
#     allocation_method   = "Dynamic"

#     tags = {
#         ORG     = "AOT"
#         ORG2    = "DMV"
#         ENV     = "TEST"
#         PROJECT = "CVO"
#     }
# }

# Configure network interfaces
resource "azurerm_network_interface" "test-sql-nic1" {
    #count = 1
    name = "test-sql-vm0-nic1"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    # network_security_group_id = "${azurerm_network_security_group.aot-cvo-test-sql-nsg.id}"
    internal_dns_name_label = "test-sql-vm0"

    ip_configuration {
        name = "test-sql-vm0-nic1-config"
        subnet_id = "${var.test_subnet_id}"
        private_ip_address_allocation = "Dynamic"
        # public_ip_address_id = "${element(azurerm_public_ip.aot-cvo-test-sql-pub-ip.id, count.index)}"  # Use if count > 1
        # public_ip_address_id = "${azurerm_public_ip.aot-cvo-test-sql-pub-ip.id}"
    }

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

# Build virtual machines
resource "azurerm_virtual_machine" "test-sql" {
    #count = 1
    name = "test-sql-vm0"
    availability_set_id   = "${azurerm_availability_set.aot-cvo-test-sql-avset.id}"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    # network_interface_ids = ["${element(azurerm_network_interface.test-sql-nic1.id, count.index)}"]  # Use if count > 1
    network_interface_ids = ["${azurerm_network_interface.test-sql-nic1.id}"]
    vm_size = "${var.machine_size["e4v3"]}"  # e4v3 is not compatible with Premium_LRS storage, use Standard_LRS or StandardSSD_LRS

    storage_image_reference {
        publisher = "${var.image_publisher}"
        offer = "${var.image_offer}"
        sku = "${var.image_sku}"
        version = "${var.image_version}"
    }

    storage_os_disk {
        name = "test-sql-vm0-os"
        caching = "None"
        create_option = "FromImage"
        managed_disk_type = "StandardSSD_LRS"
    }

    storage_data_disk {
        lun = 0
        name = "test-sql-vm0-sql"
        caching = "None"
        create_option = "Empty"
        managed_disk_type = "StandardSSD_LRS"
        disk_size_gb = 150
    }

    storage_data_disk {
        lun = 1
        name = "test-sql-vm0-log"
        caching = "None"
        create_option = "Empty"
        managed_disk_type = "Standard_LRS"
        disk_size_gb = 50
    }

    storage_data_disk {
        lun = 2
        name = "test-sql-vm0-backup"
        caching = "None"
        create_option = "Empty"
        managed_disk_type = "StandardSSD_LRS"
        disk_size_gb = 450
    }

    os_profile_windows_config {
        enable_automatic_upgrades = false
        provision_vm_agent = true
        timezone = "Eastern Standard Time"
    }

    os_profile {
        computer_name = "DSVS-DBT0"
        admin_username = "${var.admin_username}"
        admin_password = "${var.admin_password}"
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${var.diag-storage-account}"
    }

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

