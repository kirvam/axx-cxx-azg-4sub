resource "azurerm_availability_set" "aot-cvo-fcr-app-avset" {
    name = "aot-cvo-fcr-app-avset"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    platform_fault_domain_count  = 2
    platform_update_domain_count = 2
    managed                      = true
}

# resource "azurerm_public_ip" "aot-cvo-fcr-app-pub-ip" {
#     count = 2
#     name                = "aot-cvo-fcr-app-vm${count.index}-pub-ip"
#     location            = "${var.vm-location}"
#     resource_group_name = "${var.vm-rg}"
#     allocation_method   = "Dynamic"

#     tags = {
#         ORG     = "AOT"
#         ORG2    = "DMV"
#         ENV     = "TEST"
#         PROJECT = "CVO"
#     }
# }

resource "azurerm_network_interface" "fcr-app-nic1" {
    count = 2
    name = "fcr-app-vm${count.index}-nic1"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    # network_security_group_id = "${azurerm_network_security_group.aot-cvo-fcr-app-nsg.id}"
    internal_dns_name_label = "fcr-app-vm${count.index}"

    ip_configuration {
        name = "fcr-app-vm${count.index}-nic1-config"
        subnet_id = "${var.fcr_subnet_id}"
        private_ip_address_allocation = "Dynamic"
        # public_ip_address_id = "${element(azurerm_public_ip.aot-cvo-fcr-app-pub-ip.*.id, count.index)}"
    }

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

resource "azurerm_virtual_machine" "fcr-app" {
    count = 2
    name = "fcr-app-vm${count.index}"
    availability_set_id   = "${azurerm_availability_set.aot-cvo-fcr-app-avset.id}"
    location = "${var.vm-location}"
    resource_group_name = "${var.vm-rg}"
    network_interface_ids = ["${element(azurerm_network_interface.fcr-app-nic1.*.id, count.index)}"]
    vm_size = "${var.machine_size["d4sv3"]}"

    storage_image_reference {
        publisher = "${var.image_publisher}"
        offer = "${var.image_offer}"
        sku = "${var.image_sku}"
        version = "${var.image_version}"
    }

    storage_os_disk {
        name = "fcr-app-vm${count.index}-os"
        caching = "None"
        create_option = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_data_disk {
        lun = 0
        name = "fcr-app-vm${count.index}-data"
        caching = "None"
        create_option = "Empty"
        managed_disk_type = "Premium_LRS"
        disk_size_gb = 100
    }

    os_profile_windows_config {
        enable_automatic_upgrades = false
        provision_vm_agent = true
        timezone = "Eastern Standard Time"
    }

    os_profile {
        computer_name = "DSVS-FCR${count.index + 1}"
        admin_username = "${var.admin_username}"
        admin_password = "${var.admin_password}"
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${var.diag-storage-account}"
    }

    tags = {
        ORG     = "AOT"
        ORG2    = "DMV"
        ENV     = "TEST"
        PROJECT = "CVO"
    }
}

