variable "vm-location" {}

variable "vm-rg" {}

variable "machine_size" {
    type = "map"
}

variable "diag-storage-account" {}

variable "admin_username" {}
variable "admin_password" {}

variable "image_publisher" {}

variable "image_offer" {}

variable "image_sku" {}

variable "image_version" {}

# subnets too

variable "fcr_subnet_id" {}
variable "test_subnet_id" {}
variable "files_subnet_id" {}

